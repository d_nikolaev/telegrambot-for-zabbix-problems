# Telegrambot, публикующий в телеграм-канале данные о неподтверждённых проблемах в Zabbix за последние 3 дня  

![](result.png "Результат")  

Что делает скрипт:  
- ходит в Vault за логином и паролем от учётной записи телеком-бота в Zabbbix  
- забирает с помощью API данные о неподтверждённых проблемах за последние 3 дня из Zabbix  
- формирует сообщение для телеграм-канала в формате Markdown  
- идёт в Vault за токеном от телеграм-бота  
- публикует новое/редактирует уже имеющееся сообщение в телеграм-канале  

Запуск скрипта осуществляется раз в 15 секунд.  

# Начало работы  
1. Перед использованием укажите корректные данные в файлах **app/lib/telegrambot.py** и **app/lib/zabbix.py** (см. комментарии в файлах)  
2. Позаботиться о наличии переменных окружения **VAULT_TOKEN** и **VAULT_ADDRESS**  
3. Указать корректные данные в файле **.gitlab-ci.yml** (см. комментарии)  
4. Заполнить корретно файл **data/settings.json**

---
# Использование CI/CD (так работает сейчас)  
Скрипт крутится внутри контейнера на раннере.  

В случае внесения изменений в файлы программы образ будет пересобран, контейнер автоматически перезапущен.  

# Установка и использование, если это не CI/CD     

## Установка и использование с помощью docker
`cd ~`  
`git clone https://gitlab.com/d_nikolaev/telegrambot-for-zabbix-problems.git`  
`# заполните data/settings.json`  
`docker build -t telegrambot-for-zabbix-problems .`  
`docker run -e VAULT_ADDRESS=<> -e VAULT_TOKEN=<> --name telegrambot-for-zabbix-problems --restart always telegrambot-for-zabbix-problems`  
_вместо <> указать корректные данные_   


## Установка в Linux без использования docker (! НЕ ПРОВЕРЯЛОСЬ !)   
### 1. Установить _Python3.9+_  

### 2. Установить в случае необходимости _git_  

### 3. Склонировать репозиторий, установить окружение и требуемые библиотеки  
`cd ~`  
`git clone https://gitlab.com/d_nikolaev/telegrambot-for-zabbix-problems.git`  
`cd telegrambot-for-zabbix-problems`  
`python3 -m venv venv`  
`source venv/bin/activate`  
`pip install --upgrade pip`  
`pip install -r requirements.txt`  

### 4. Открываем на редактирование файл data/settings.json и подставляем свои значения, сохраняем  
`{`  
`  "CHANNEL_USERNAME": "<ID_or_USERNAME_of_channel>",`  
`  "MESSAGE_ID": 0`  
`}`  

### 5. Создадим новый сервис для работы бота. Создаём файл, открываем его на редактирование
`touch /etc/systemd/system/zabbixbot.service`  
`chmod 664 /etc/systemd/system/zabbixbot.service`  
`vi /etc/systemd/system/zabbixbot.service`  

### Вставляем в файл следующий текст  
`[Unit]`  
`Description=Telegrambot for zabbix-problems`  
`After=network.target`  
`[Service]`  
`Type=simple`  
`User=root`  
`ExecStart=~/telegrambot-for-zabbix-problems/app/main.py`  
`[Install]`  
`WantedBy=multi-user.target`  

### Стартуем новый сервис, проверяем статус  
`systemctl start zabbixbot.service`  
`systemctl status zabbixbot.service`  

### Добавляем его в автозагрузку  
`systemctl enable zabbixbot.service`  

