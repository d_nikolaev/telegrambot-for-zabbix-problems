import json
from pathlib import Path

from pydantic import BaseModel, HttpUrl
from pydantic_settings import BaseSettings


SETTINGS_PATH = 'data/settings.json'


class Env(BaseSettings):
    vault_token: str
    vault_address: HttpUrl


env = Env()


with open(SETTINGS_PATH) as f:
    data = json.load(f)


class Data(BaseModel):
    CHANNEL_USERNAME: str
    MESSAGE_ID: int

    def save(self):
        Path(SETTINGS_PATH).write_text(self.model_dump_json(indent=2), encoding='utf-8')


data = Data.model_validate(data)