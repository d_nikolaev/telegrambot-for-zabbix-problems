import hvac
import telebot

from conf.settings import env, data


##### ПОМЕНЯТЬ В СЛУЧАЕ НЕОБХОДИМОСТИ ЭТИ ПАРАМЕТРЫ
path = "/automation/telegrambot"
mount_point = '/secrets'
#### 


class Bot():
    def __init__(self) -> None:
        self._token = self.__getToken()
        self.bot = telebot.TeleBot(self._token)
    

    def __getToken(self) -> str:
        client = hvac.Client(url=env.vault_address, token=env.vault_token, verify=False)
        response = client.secrets.kv.read_secret_version(mount_point=mount_point, path=path, raise_on_deleted_version=True)
        return response['data']['data']['token']
    

    async def sendInfoToChannel(self, text: str):
        """Отправить сообщение в канал

        Args:
            text (str): сообщение
        """
        if not data.MESSAGE_ID:
            msg = self.bot.send_message(data.CHANNEL_USERNAME, text, parse_mode='Markdown')
            data.MESSAGE_ID = msg.message_id
            data.save()
        else:
            self.bot.edit_message_text(text, data.CHANNEL_USERNAME, data.MESSAGE_ID, parse_mode='Markdown')
