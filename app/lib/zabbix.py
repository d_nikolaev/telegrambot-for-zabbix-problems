import sys
import os
sys.path.append(os.getcwd())
import datetime
import time
from typing import List, Dict

import hvac
from pyzabbix import ZabbixAPI
from prettytable import PrettyTable
from urllib3 import disable_warnings, exceptions # для отключения сообщений Warning о небезопасности сертификата

from app.conf.settings import env


disable_warnings(exceptions.InsecureRequestWarning) # Отключаем предупреждения Warning о небезопасности сертификата

##### ПОМЕНЯТЬ В СЛУЧАЕ НЕОБХОДИМОСТИ ЭТИ ПАРАМЕТРЫ
path = "/automation/zabbix"
mount_point = '/secrets'
#### 


class Zabbix():
    def __init__(self) -> None:
        self._login = self.__getKey("login")
        self._password = self.__getKey("password")
        self._zapi = self.__getZapi()


    def __getKey(self, key: str) -> str:
        client = hvac.Client(url=env.vault_address, token=env.vault_token, verify=False)
        response = client.secrets.kv.read_secret_version(mount_point=mount_point, path=path, raise_on_deleted_version=True)
        return response['data']['data'][key]


    def __getZapi(self) -> ZabbixAPI:
        zapi = ZabbixAPI('https://zabbix.corp.samsonopt.ru/')
        zapi.session.verify = False
        zapi.login(user=self._login, password=self._password)
        return zapi


    class Problem():
        """Класс Проблема

        Нужен для конвертации данных по триггерам из Zabbix в более-менее читабельную сущность. Приближено к сообщению на доске мониторинга
        """
        def __init__(self, trigger: Dict) -> None:
            self.host = self.__getHost(trigger)
            self.lastvalue = self.__getLastvalue(trigger)
            self.descripion = self.__getDescriptionProblem(trigger)
            self.time = self.__getTime(trigger)
            self.priority = self.__getPriority(trigger)

        def __getHost(self, trigger: Dict) -> str:
            return trigger['hosts'][0]['host']
        
        def __getLastvalue(self, trigger: Dict) -> str:
            return trigger['items'][0]['lastvalue']
        
        def __getDescriptionProblem(self, trigger: Dict) -> str:
            description = trigger['description'].replace('{HOSTNAME}', self.host).replace('{ITEM.LASTVALUE}', self.lastvalue)
            description = description.replace('{HOST.NAME}', self.host).replace('{$TEMP_CRIT:"Device"}','')
            description = description.replace('<>', '!=')
            return description
        
        def __getTime(self, trigger: Dict) -> str:
            return datetime.datetime.fromtimestamp(int(trigger['lastchange'])).strftime('%d.%m %H:%M') #### ('%d.%m.%Y %H:%M:%S')

        def __getPriority(self, trigger: Dict) -> str:
            prioritySymbol = {'1': "🔵",    # Значок приоритета тригера
                            '2': "🟡", 
                            '3': "🟠", 
                            '4': "🔴", 
                            '5': "🆘"}
            return prioritySymbol[trigger['priority']]


    def getProblems(self) -> List[Problem]:
        three_days_ago = int(time.time()) - 259200                                #### получаем UNIX-time, сегодня минус 3 дня (в секундах = 259200)
        problems = self._zapi.trigger.get(min_severity=2,                         #### Возврат только тех триггеров, у которых важность больше или равна заданной важности
                                          filter={'value': 1},                    #### Триггеры в состоянии проблема
                                          selectHosts='extend',                   #### Возврат узлов сети, которым принадлежит триггер, в свойстве hosts
                                          lastChangeSince=three_days_ago,         #### Возврат только тех триггеров, которые изменили своё состояние после заданного времени
                                          selectItems='extend',                   #### Возврат элементов данных, которые содержатся в выражении триггера, в свойстве items
                                          sortfield="lastchange",                 #### Сортировка результата в соответствии с заданными свойствами.
                                          sortorder="DESC",                       #### Порядок сортировки
                                          withLastEventUnacknowledged='extend')   #### Возврат только тех триггеров, последние события которых неподтверждены
        problems = list(map(self.Problem, problems))                              #### Конвертируем полученные результаты в экземпляры класса Problem
        return problems
    

    def getReply(self, problems: List[Problem] = None) -> str:
        problems = self.getProblems() if problems is None else problems
        reply = f"*Неподтверждённые проблемы за последние 3 дня*\n{str(datetime.datetime.now().strftime('%d.%m.%Y %H:%M:%S'))}\n\n"
        for problem in problems:
            reply += f"{problem.priority}_{problem.time}_   *{problem.host}*  `{problem.descripion}`\n"
            if len(reply) >= 3950:
                reply += "\n and more..."
                break
        return reply


def main():
    zabbix = Zabbix()
    problems = zabbix.getProblems()
    answer_list = []
    for problem in problems:
        answer_list.append(problem.priority)
        answer_list.append(problem.time)
        answer_list.append(problem.host)
        #answer_list.append(problem.lastvalue)
        answer_list.append(problem.descripion)
    th = ["Приоритет", "Дата", "Хост", "Проблема"]
    columns = len(th)
    table = PrettyTable(th)
    td_data = answer_list[:]
    while td_data:
        # Используя срез, добавляем первые 4 элемента в строку (columns = 4).
        table.add_row(td_data[:columns])
        # Используя срез, переопределяем td_data так, чтобы он больше не содержал первые 4 элемента.
        td_data = td_data[columns:]
    print(table)


if __name__ == '__main__':
    main()
