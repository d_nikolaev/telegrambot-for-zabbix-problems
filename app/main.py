import asyncio
import logging
from requests.exceptions import ReadTimeout, ConnectionError

from lib.telegrambot import Bot
from lib.zabbix import Zabbix
from telebot.apihelper import ApiTelegramException


file_log = logging.FileHandler("bot.log")
console_out = logging.StreamHandler()
logging.basicConfig(handlers=(file_log, console_out),
                    format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO
                    )


async def main_loop():
    zabbix = Zabbix()
    telegram_bot = Bot()
    while True:
        try:
            await telegram_bot.sendInfoToChannel(text=zabbix.getReply())
        except (ApiTelegramException, ConnectionError, ReadTimeout) as e:
            logger.info("ApiTelegramException: ReadTimeout or ConnectionError. Pause 5 sec.")
            await asyncio.sleep(5)
            continue
        await asyncio.sleep(15) #### поменять на 30 в случае необходимости


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.create_task(main_loop())
    loop.run_forever()